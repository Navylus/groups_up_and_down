import React, { Component } from 'react';
import { Button, TextField, Select, MenuItem } from "@material-ui/core"
import { DndProvider } from 'react-dnd'
// import DND from "./dnd"
// import Backend from 'react-dnd-html5-backend'
import "./style.css"

const MATCH_TO_WIN = 3
const MAX_LEVEL = 3
const MIN_LEVEL = 0

class MainComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newLevel: null,
            newName: null,
            newDiv: null,
            groups: [
                {
                    name: "Diam +",
                    level: 3,
                    players: []
                },
                {
                    name: "Plat / Gold",
                    level: 2,
                    players: []
                },
                {
                    name: "Gold -",
                    level: 1,
                    players: []
                },
            ],
            players: [
                {
                    name: "Navy",
                    div: "Gold",
                    level: 2,
                    score: 0,
                    looses: 0,
                    wins: 0,
                },
                {
                    name: "Elias",
                    div: "Plat",
                    level: 2,
                    score: 0,
                    looses: 0,
                    wins: 0,
                },
                {
                    name: "Seilkaz",
                    div: "GM",
                    level: 3,
                    score: 0,
                    looses: 0,
                    wins: 0,
                },
                {
                    name: "rob",
                    div: "Diamant",
                    level: 3,
                    score: 0,
                    looses: 0,
                    wins: 0,
                },
            ]
        }
    }

    renderList = () => {
        const { players } = this.state
        const res = []
        players.forEach(element => {
            res.push(<div className="containerName" key={element.name}>
                <div className="player__name">{element.name}</div>
                <div>{element.div}</div>
            </div>)
        });
        return res
    }

    saveData = () => {
        const { groups, players } = this.state
        const downloadFile = async () => {
            const myData = { groups, players };
            const fileName = "savedPlayersAndGroups";
            const json = JSON.stringify(myData);
            const blob = new Blob([json], { type: 'application/json' });
            const href = await URL.createObjectURL(blob);
            const link = document.createElement('a');
            link.href = href;
            link.download = fileName + ".json";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
        downloadFile()
    }

    createGroups = (players) => {
        const { groups } = this.state

        groups.forEach(gr => {
            gr.players = []
            players.forEach(e => {
                if (gr.level === e.level) {
                    gr.players.push(e)
                }
            })
        })
        localStorage.setItem('dataPlayersGroups', { players, groups })
        this.setState({ groups })
    }

    winAMatch = (name) => {
        const { players } = this.state

        players.map(player => {
            if (player.name === name) {
                player.score++
                player.wins++
                if (player.score < 0) {
                    player.score = 1
                }
                if (player.score >= MATCH_TO_WIN) {
                    if (player.level !== MAX_LEVEL) {
                        player.level++
                        player.score = 0
                    }
                    this.setState({ players })
                    this.createGroups(players)
                } else {
                    this.setState({ players })
                }
            }
        })
    }

    looseAMatch = (name) => {
        const { players } = this.state

        players.map(player => {
            if (player.name === name) {
                player.score--
                player.looses++
                if (player.score > 0) {
                    player.score = -1
                }
                if (player.score <= -MATCH_TO_WIN) {
                    if (player.level !== MIN_LEVEL) {
                        player.level--
                        player.score = 0
                    }
                    this.setState({ players })
                    this.createGroups(players)
                } else {
                    this.setState({ players })
                }
            }
        })
    }

    renderGroups = () => {
        const { groups } = this.state
        groups.sort((a, b) => {
            return a.level < b.level ? -1 : 0
        })
        const res = []
        groups.reverse()
        groups.forEach(e => {
            if (e.players.length > 0) {
                res.push(
                    <div className="groupContainer">
                        <h2>{e.name}</h2>
                        <div className="player__group__list">
                            {
                                e.players.map(player => {
                                    return (
                                        <div className="player__desc">
                                            <div className="player__desc__name">{player.name}</div>
                                            <div className="player__desc__score">{player.score}</div>
                                            <div className="player__desc__wins">{`Wins: ${player.wins}`}</div>
                                            <div className="player__desc__looses">{`Looses: ${player.looses}`}</div>
                                            <Button color="primary" onClick={() => { this.winAMatch(player.name) }}>
                                                Win
                                            </Button>
                                            <Button color="secondary" onClick={() => { this.looseAMatch(player.name) }}>
                                                Loose
                                            </Button>
                                        </div>
                                    )
                                })
                            }
                        </div>
                    </div>
                )
            }
        })
        return res
    }

    addPlayer = () => {
        const { newLevel, newDiv, newName, players } = this.state
        if (newDiv === null || newLevel === null || newName === null) return false
        players.push({
            name: newName,
            div: newDiv,
            level: newLevel,
            score: 0,
            looses: 0,
            wins: 0,
        })
        this.setState({ newName: "", newDiv: "", players })
    }

    render() {
        const { players, groups, newLevel, newName, newDiv } = this.state
        return (
            <div>
                <div className="player__holder">
                    {this.renderList()}
                </div>
                <div className="list__holder">
                    <div className="form__holder">
                        <TextField label="Player name" value={newName} onChange={(e) => { this.setState({ newName: e.target.value }) }}></TextField >
                        <TextField label="Player level" value={newDiv} onChange={(e) => { this.setState({ newDiv: e.target.value }) }}></TextField >
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={newLevel}
                            placeholder="god"
                            required
                            onChange={(e) => { this.setState({ newLevel: e.target.value }) }}
                        >
                            {groups.map(grp => <MenuItem value={grp.level}>{grp.name}</MenuItem>)}
                        </Select>
                        <Button color="primary" onClick={() => this.addPlayer()}>
                            ADD PLAYER
                        </Button>
                    </div>
                    <Button onClick={() => this.createGroups(players)} color="secondary" variant="contained">
                        Generate Groups
                    </Button>
                </div>
                <div className="group__holder">
                    {this.renderGroups()}
                </div>
                <Button onClick={() => this.saveData()} color="primary" className="save__btn">SAVE DATA</Button>
                {/* <DndProvider backend={Backend}>
                    <DND />
                </DndProvider> */}
            </div>
        );
    }
}

export default MainComponent;




