import React, { useMemo } from 'react'
import { useState, useCallback } from 'react'
import { NativeTypes } from 'react-dnd-html5-backend'
import { useDrop } from 'react-dnd'

const DND = () => {
    const [droppedFiles, setDroppedFiles] = useState([])
    const handleFileDrop = useCallback((item, monitor) => {
        if (monitor) {
            const files = monitor.getItem().files
            setDroppedFiles(files)
        }
    }, [])
    return (
        <>
            <TargetBox onDrop={handleFileDrop} />
            <FileList files={droppedFiles} />
        </>
    )
}
export default DND

function list(files) {
    const file = files[0]
    console.log(files[0])
    const reader = new FileReader();
    reader.readAsText(file);
    console.log(reader.result)
}


const FileList = ({ files }) => {
    if (files.length === 0) {
        return <div>Nothing to display</div>
    }
    const fileList = list(files)
    return <div>{fileList}</div>
}

const style = {
    border: '1px solid gray',
    height: '15rem',
    width: '15rem',
    padding: '2rem',
    textAlign: 'center',
}


const TargetBox = (props) => {
    const { onDrop } = props
    const [{ canDrop, isOver }, drop] = useDrop({
        accept: [NativeTypes.FILE],
        drop(item, monitor) {
            if (onDrop) {
                onDrop(props, monitor)
            }
        },
        collect: (monitor) => ({
            isOver: monitor.isOver(),
            canDrop: monitor.canDrop(),
        }),
    })
    const isActive = canDrop && isOver
    return (
        <div ref={drop} style={style}>
            {isActive ? 'Release to drop' : 'Drag file here'}
        </div>
    )
}
